﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VantageMassFetch
{
    public partial class Form1 : Form
    {
        private string DIALOG_TITLE_ERROR = "";
        private const string S3_ROOT_FOLDER = "http://s3.amazonaws.com/Avalere";

        public Form1()
        {
            InitializeComponent();
            txtReportYear.Text = Convert.ToString(DateTime.Now.Year - 2);
            this.Text = String.Format("{0} ({1})", Application.ProductName, Application.ProductVersion);
            DIALOG_TITLE_ERROR = String.Format("{0}: Error", Application.ProductName);
        }

        private void btnFilePick_Click(object sender, EventArgs e)
        {
            DialogResult drFile = dlgGetFile.ShowDialog(this);
            if (drFile == System.Windows.Forms.DialogResult.OK)
            {
                txtCSVManifest.Text = dlgGetFile.FileName;
                btnSaveLog.Enabled = false;
                GoDo();
                btnSaveLog.Enabled = true;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GoDo();
        }

        private void btnPickSavePath_Click(object sender, EventArgs e)
        {
            dlgGetFolder.RootFolder = Environment.SpecialFolder.Desktop;
            dlgGetFolder.ShowNewFolderButton = true;
            dlgGetFolder.SelectedPath = string.IsNullOrEmpty(txtSavePath.Text.Trim()) ? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) : txtSavePath.Text.Trim();
            DialogResult drFolder = dlgGetFolder.ShowDialog();
            if (drFolder == System.Windows.Forms.DialogResult.OK)
            {
                txtSavePath.Text = dlgGetFolder.SelectedPath;
            }
        }

        private void GoDo()
        {
            bool bFileExists = false;
            bool bFolderExists = false;
            string strManifest = txtCSVManifest.Text.Trim();
            string strSavePath = txtSavePath.Text.Trim();
            string strColName = txtColName.Text.Trim();
            WebClient wcDownloader = new WebClient();

            try
            {
                if (!string.IsNullOrEmpty(strManifest) && File.Exists(strManifest))
                {
                    bFileExists = true;
                }
            }
            catch
            {
                bFileExists = false;
            }

            try
            {
                if (!string.IsNullOrEmpty(strSavePath) && Directory.Exists(strSavePath))
                {
                    bFolderExists = true;
                }
            }
            catch
            {
                bFolderExists = false;
            }

            if (!string.IsNullOrEmpty(strColName) && bFileExists && bFolderExists)
            {
                int iLine = 0;
                int IdField = -1;
                int MinReqLength = -1;
                string[] headers;
                List<string> FilenameCache = new List<string>();
                string strDataItem = "";
                foreach (string Line in File.ReadLines(strManifest))
                {
                    iLine++;
                    if (iLine == 1)
                    {
                        headers = Line.Split(",".ToCharArray());
                        if (headers.Length == 1)
                        {
                            IdField = 0;
                        }
                        else
                        {
                            for (int i = 0; i < headers.Length; i++)
                            {
                                if (headers[i].Trim().Equals(strColName, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    IdField = i;
                                    break;
                                }
                            }
                        }
                        MinReqLength = IdField + 1;
                    }
                    else
                    {
                        if (IdField < 0)
                        {
                            MessageBox.Show("Unable to find Medicare Id field!", DIALOG_TITLE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        else
                        {
                            string[] data = Line.Split(",".ToCharArray());
                            if (data.Length >= MinReqLength)
                            {
                                strDataItem = data[IdField].Trim();
                                strDataItem = strDataItem.Replace("-","");
                                strDataItem = strDataItem.Replace("\"", "");
                                if (!FilenameCache.Contains(strDataItem))
                                {
                                    FilenameCache.Add(strDataItem);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Data of unexpected length.", DIALOG_TITLE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                        }
                    }
                }
                if (FilenameCache.Count <= 0)
                {
                    MessageBox.Show("Manifest did not yield any data to look up in S3.", DIALOG_TITLE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    txtOutput.Text += "Beginning download...\r\n";
                    foreach (string strFilename in FilenameCache)
                    {
                        if (chkReadmissions.Checked)
                        {
                            txtOutput.Text += String.Format("{0}\r\n", DownloadContent(wcDownloader, strSavePath, String.Format("{0}%20Readmissions%20Reports", txtReportYear.Text), String.Format("{0}-Readmissions-{1}.xlsx", txtReportYear.Text, strFilename)));
                        }
                        if (chkMarketShare.Checked)
                        {
                            txtOutput.Text += String.Format("{0}\r\n", DownloadContent(wcDownloader, strSavePath, String.Format("{0}%20Market%20Share", txtReportYear.Text), String.Format("{0}-MktShare-{1}.xlsx", txtReportYear.Text, strFilename)));
                        }
                    }
                    txtOutput.Text += "Download completed.\r\n";
                }
            }
            else
            {
                MessageBox.Show("Please enter the column name and pick the import manifest", DIALOG_TITLE_ERROR, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string DownloadContent(WebClient FileDownloader, string LocalPath, string S3FolderName, string S3Filename)
        {
            string strRetVal = "";
            string ReqFilename = String.Format("{0}/{1}/{2}", S3_ROOT_FOLDER, S3FolderName, S3Filename);
            string ReqLocalPath = LocalPath.Trim();
            if (ReqLocalPath.EndsWith(@"\"))
            {
                ReqLocalPath = String.Format("{0}{1}", ReqLocalPath, S3Filename);
            }
            else
            {
                ReqLocalPath = String.Format(@"{0}\{1}", ReqLocalPath, S3Filename);
            }

            bool bShouldDownload = true;
            if (chkOverwrite.Checked)
            {
                if (File.Exists(ReqLocalPath))
                {
                    try
                    {
                        File.Delete(ReqLocalPath);
                        strRetVal = String.Format("Predownload: {0} [File deleted successfully]", ReqFilename);

                    }
                    catch
                    {
                        bShouldDownload = false;
                        strRetVal = String.Format("predownload: {0} [Failed to delete]", ReqFilename);
                    }
                }
            }
            else
            {
                if (File.Exists(ReqLocalPath))
                {
                    bShouldDownload = false;
                    strRetVal = String.Format("Predownload: {0} [File already exists]", ReqFilename);
                }
            }

            if (bShouldDownload)
            {
                try
                {
                    FileDownloader.DownloadFile(ReqFilename, ReqLocalPath);
                    strRetVal = String.Format("Downloading: {0} [Success]", ReqFilename);
                }
                catch (Exception ex)
                {
                    strRetVal = String.Format("Downloading: {0} [Error: {1}]", ReqFilename, ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                }
            }

            return strRetVal;
        }

        private void btnSaveLog_Click(object sender, EventArgs e)
        {
            string strLogFilename = txtSavePath.Text.Trim();

            if (Directory.Exists(strLogFilename))
            {
                int iPos = -1;
                string strOutLogLine = "";
                string strReportName = "";
                string strReportType = "";
                strLogFilename = String.Format(@"{0}\log.txt", strLogFilename).Replace(@"\\", @"\");
                try
                {
                    StreamWriter swLog = File.CreateText(strLogFilename);
                    foreach(string Line in txtOutput.Lines)
                    {
                        strReportName = "";
                        iPos = Line.LastIndexOf(".xlsx", StringComparison.CurrentCultureIgnoreCase);
                        if (iPos > 0)
                        {
                            iPos = iPos - 6;
                            strReportName = Line.Substring(iPos, 6);
                        }

                        strReportType = "";
                        iPos = Line.LastIndexOf("readmissions", StringComparison.CurrentCultureIgnoreCase);
                        if (iPos > 0)
                        {
                            strReportType = "Readmissions";
                        }
                        else
                        {
                            iPos = Line.LastIndexOf("market%20share", StringComparison.CurrentCultureIgnoreCase);
                            if (iPos > 0)
                            {
                                strReportType = "Market Share";
                            }
                            else
                            {
                                iPos = Line.LastIndexOf("length%20of%20stay", StringComparison.CurrentCultureIgnoreCase);
                                if (iPos > 0)
                                {
                                    strReportType = "Length of Stay";
                                }
                            }
                        }

                        strOutLogLine = Line;
                        if (Line.EndsWith("[File already exists]") || Line.EndsWith("[Success]"))
                        {
                            strOutLogLine = String.Format("Report for [{0}-{1}] downloaded.", strReportName, strReportType);
                        }
                        else if (Line.EndsWith("(404) Not Found.]"))
                        {
                            strOutLogLine = String.Format("Report for [{0}-{1}] doesn't exist.", strReportName, strReportType);
                        }
                        swLog.WriteLine(strOutLogLine);
                    }
                    swLog.Close();
                }
                catch (Exception ex)
                {
                    txtOutput.Text += String.Format("Could not write log file: {0}", ex.Message);
                }
            }
            else
            {
                txtOutput.Text += "Could not write log file: Directory does not exist!";
            }
        }
    }
}
