﻿namespace VantageMassFetch
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCSVManifest = new System.Windows.Forms.Label();
            this.txtCSVManifest = new System.Windows.Forms.TextBox();
            this.dlgGetFile = new System.Windows.Forms.OpenFileDialog();
            this.btnFilePick = new System.Windows.Forms.Button();
            this.lblColName = new System.Windows.Forms.Label();
            this.txtColName = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblInclude = new System.Windows.Forms.Label();
            this.chkReadmissions = new System.Windows.Forms.CheckBox();
            this.chkMarketShare = new System.Windows.Forms.CheckBox();
            this.chkLengthOfStay = new System.Windows.Forms.CheckBox();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.lblSaveFilesIn = new System.Windows.Forms.Label();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.btnPickSavePath = new System.Windows.Forms.Button();
            this.dlgGetFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.chkOverwrite = new System.Windows.Forms.CheckBox();
            this.btnSaveLog = new System.Windows.Forms.Button();
            this.txtReportYear = new System.Windows.Forms.TextBox();
            this.lblReportYear = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCSVManifest
            // 
            this.lblCSVManifest.AutoSize = true;
            this.lblCSVManifest.Location = new System.Drawing.Point(18, 130);
            this.lblCSVManifest.Name = "lblCSVManifest";
            this.lblCSVManifest.Size = new System.Drawing.Size(74, 13);
            this.lblCSVManifest.TabIndex = 0;
            this.lblCSVManifest.Text = "CSV Manifest:";
            // 
            // txtCSVManifest
            // 
            this.txtCSVManifest.Location = new System.Drawing.Point(96, 130);
            this.txtCSVManifest.Name = "txtCSVManifest";
            this.txtCSVManifest.Size = new System.Drawing.Size(506, 20);
            this.txtCSVManifest.TabIndex = 1;
            // 
            // btnFilePick
            // 
            this.btnFilePick.Location = new System.Drawing.Point(608, 126);
            this.btnFilePick.Name = "btnFilePick";
            this.btnFilePick.Size = new System.Drawing.Size(43, 23);
            this.btnFilePick.TabIndex = 2;
            this.btnFilePick.Text = "Pick";
            this.btnFilePick.UseVisualStyleBackColor = true;
            this.btnFilePick.Click += new System.EventHandler(this.btnFilePick_Click);
            // 
            // lblColName
            // 
            this.lblColName.AutoSize = true;
            this.lblColName.Location = new System.Drawing.Point(18, 16);
            this.lblColName.Name = "lblColName";
            this.lblColName.Size = new System.Drawing.Size(135, 13);
            this.lblColName.TabIndex = 3;
            this.lblColName.Text = "Medicare Id Column Name:";
            // 
            // txtColName
            // 
            this.txtColName.Location = new System.Drawing.Point(159, 13);
            this.txtColName.Name = "txtColName";
            this.txtColName.Size = new System.Drawing.Size(189, 20);
            this.txtColName.TabIndex = 4;
            this.txtColName.Text = "MedicareId";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(657, 126);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(73, 23);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lblInclude
            // 
            this.lblInclude.AutoSize = true;
            this.lblInclude.Location = new System.Drawing.Point(18, 45);
            this.lblInclude.Name = "lblInclude";
            this.lblInclude.Size = new System.Drawing.Size(45, 13);
            this.lblInclude.TabIndex = 6;
            this.lblInclude.Text = "Include:";
            // 
            // chkReadmissions
            // 
            this.chkReadmissions.AutoSize = true;
            this.chkReadmissions.Checked = true;
            this.chkReadmissions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReadmissions.Location = new System.Drawing.Point(95, 44);
            this.chkReadmissions.Name = "chkReadmissions";
            this.chkReadmissions.Size = new System.Drawing.Size(91, 17);
            this.chkReadmissions.TabIndex = 7;
            this.chkReadmissions.Text = "Readmissions";
            this.chkReadmissions.UseVisualStyleBackColor = true;
            // 
            // chkMarketShare
            // 
            this.chkMarketShare.AutoSize = true;
            this.chkMarketShare.Checked = true;
            this.chkMarketShare.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMarketShare.Location = new System.Drawing.Point(193, 45);
            this.chkMarketShare.Name = "chkMarketShare";
            this.chkMarketShare.Size = new System.Drawing.Size(90, 17);
            this.chkMarketShare.TabIndex = 8;
            this.chkMarketShare.Text = "Market Share";
            this.chkMarketShare.UseVisualStyleBackColor = true;
            // 
            // chkLengthOfStay
            // 
            this.chkLengthOfStay.AutoSize = true;
            this.chkLengthOfStay.Enabled = false;
            this.chkLengthOfStay.Location = new System.Drawing.Point(290, 45);
            this.chkLengthOfStay.Name = "chkLengthOfStay";
            this.chkLengthOfStay.Size = new System.Drawing.Size(95, 17);
            this.chkLengthOfStay.TabIndex = 9;
            this.chkLengthOfStay.Text = "Length of Stay";
            this.chkLengthOfStay.UseVisualStyleBackColor = true;
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutput.Location = new System.Drawing.Point(21, 157);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(883, 524);
            this.txtOutput.TabIndex = 10;
            // 
            // lblSaveFilesIn
            // 
            this.lblSaveFilesIn.AutoSize = true;
            this.lblSaveFilesIn.Location = new System.Drawing.Point(18, 100);
            this.lblSaveFilesIn.Name = "lblSaveFilesIn";
            this.lblSaveFilesIn.Size = new System.Drawing.Size(71, 13);
            this.lblSaveFilesIn.TabIndex = 11;
            this.lblSaveFilesIn.Text = "Save Files In:";
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(96, 97);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.Size = new System.Drawing.Size(506, 20);
            this.txtSavePath.TabIndex = 12;
            // 
            // btnPickSavePath
            // 
            this.btnPickSavePath.Location = new System.Drawing.Point(608, 97);
            this.btnPickSavePath.Name = "btnPickSavePath";
            this.btnPickSavePath.Size = new System.Drawing.Size(43, 23);
            this.btnPickSavePath.TabIndex = 13;
            this.btnPickSavePath.Text = "Pick";
            this.btnPickSavePath.UseVisualStyleBackColor = true;
            this.btnPickSavePath.Click += new System.EventHandler(this.btnPickSavePath_Click);
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.AutoSize = true;
            this.chkOverwrite.Location = new System.Drawing.Point(657, 101);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(110, 17);
            this.chkOverwrite.TabIndex = 14;
            this.chkOverwrite.Text = "Overwrite Existing";
            this.chkOverwrite.UseVisualStyleBackColor = true;
            // 
            // btnSaveLog
            // 
            this.btnSaveLog.Enabled = false;
            this.btnSaveLog.Location = new System.Drawing.Point(830, 128);
            this.btnSaveLog.Name = "btnSaveLog";
            this.btnSaveLog.Size = new System.Drawing.Size(75, 23);
            this.btnSaveLog.TabIndex = 15;
            this.btnSaveLog.Text = "Save Log";
            this.btnSaveLog.UseVisualStyleBackColor = true;
            this.btnSaveLog.Click += new System.EventHandler(this.btnSaveLog_Click);
            // 
            // txtReportYear
            // 
            this.txtReportYear.Location = new System.Drawing.Point(96, 71);
            this.txtReportYear.MaxLength = 4;
            this.txtReportYear.Name = "txtReportYear";
            this.txtReportYear.Size = new System.Drawing.Size(91, 20);
            this.txtReportYear.TabIndex = 17;
            // 
            // lblReportYear
            // 
            this.lblReportYear.AutoSize = true;
            this.lblReportYear.Location = new System.Drawing.Point(18, 74);
            this.lblReportYear.Name = "lblReportYear";
            this.lblReportYear.Size = new System.Drawing.Size(67, 13);
            this.lblReportYear.TabIndex = 16;
            this.lblReportYear.Text = "Report Year:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 693);
            this.Controls.Add(this.txtReportYear);
            this.Controls.Add(this.lblReportYear);
            this.Controls.Add(this.btnSaveLog);
            this.Controls.Add(this.chkOverwrite);
            this.Controls.Add(this.btnPickSavePath);
            this.Controls.Add(this.txtSavePath);
            this.Controls.Add(this.lblSaveFilesIn);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.chkLengthOfStay);
            this.Controls.Add(this.chkMarketShare);
            this.Controls.Add(this.chkReadmissions);
            this.Controls.Add(this.lblInclude);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.txtColName);
            this.Controls.Add(this.lblColName);
            this.Controls.Add(this.btnFilePick);
            this.Controls.Add(this.txtCSVManifest);
            this.Controls.Add(this.lblCSVManifest);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCSVManifest;
        private System.Windows.Forms.TextBox txtCSVManifest;
        private System.Windows.Forms.OpenFileDialog dlgGetFile;
        private System.Windows.Forms.Button btnFilePick;
        private System.Windows.Forms.Label lblColName;
        private System.Windows.Forms.TextBox txtColName;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lblInclude;
        private System.Windows.Forms.CheckBox chkReadmissions;
        private System.Windows.Forms.CheckBox chkMarketShare;
        private System.Windows.Forms.CheckBox chkLengthOfStay;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label lblSaveFilesIn;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Button btnPickSavePath;
        private System.Windows.Forms.FolderBrowserDialog dlgGetFolder;
        private System.Windows.Forms.CheckBox chkOverwrite;
        private System.Windows.Forms.Button btnSaveLog;
        private System.Windows.Forms.TextBox txtReportYear;
        private System.Windows.Forms.Label lblReportYear;
    }
}

